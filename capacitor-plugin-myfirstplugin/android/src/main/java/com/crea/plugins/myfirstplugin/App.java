package com.crea.plugins.myfirstplugin;
import android.content.Intent;
import com.getcapacitor.JSObject;
import com.getcapacitor.NativePlugin;
import com.getcapacitor.Plugin;
import com.getcapacitor.PluginCall;
import com.getcapacitor.PluginMethod;

import static androidx.core.content.ContextCompat.startActivity;

@NativePlugin
public class App extends Plugin {

    @PluginMethod
    public void echo(PluginCall call) {
        Intent intent = new Intent(getActivity(), MainActivity.class);
        getActivity().startActivity(intent);

        String value = call.getString("value");

        JSObject ret = new JSObject();
        ret.put("value", value);
        call.success(ret);
    }
}
