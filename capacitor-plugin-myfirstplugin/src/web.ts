import { WebPlugin } from '@capacitor/core';
import { AppPlugin } from './definitions';

export class AppWeb extends WebPlugin implements AppPlugin {
  constructor() {
    super({
      name: 'App',
      platforms: ['web'],
    });
  }

  async echo(options: { value: string }): Promise<{ value: string }> {
    console.log('ECHO', options);
    return options;
  }
}

const App = new AppWeb();

export { App };

import { registerWebPlugin } from '@capacitor/core';
registerWebPlugin(App);
