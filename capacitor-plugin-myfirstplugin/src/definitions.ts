declare module '@capacitor/core' {
  interface PluginRegistry {
    App: AppPlugin;
  }
}

export interface AppPlugin {
  echo(options: { value: string }): Promise<{ value: string }>;
}
