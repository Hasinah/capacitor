declare module '@capacitor/core' {
  interface PluginRegistry {
    ContactPlugin: ContactPluginPlugin;
  }
}

export interface ContactPluginPlugin {
  echo(options: { value: string }): Promise<{ value: string }>;
  // on get les contacts et on a jout un filter  pour filtrer les contact
  getContacts( filter: string): Promise<{ results: any[] }>;
}
