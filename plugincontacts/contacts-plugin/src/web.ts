import { WebPlugin } from '@capacitor/core';
import { ContactPluginPlugin } from './definitions';

export class ContactPluginWeb extends WebPlugin implements ContactPluginPlugin {
  constructor() {
    super({
      name: 'ContactPlugin',
      platforms: ['web'],
    });
  }
// Notre echo
  async echo(options: { value: string }): Promise<{ value: string }> {
    console.log('ECHO', options);
    return options;
  }
  //l'implementation de contact
  async getContacts(filter: string ): Promise<{ results: any[]}> {
    console.log('filter:', filter);
    return{
      results:[{
        firstName:'Hasinah',
        lastName:'Tabet',
        telephone:'12345',
      }

      ]
    }
  }
}

const ContactsPlugin = new ContactPluginWeb();

export { ContactsPlugin};
// le plugin est enregistrer et on peut l'utiliser avec ionic application
import { registerWebPlugin } from '@capacitor/core';
registerWebPlugin(ContactsPlugin);
